import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import QtQml.Models 2.15


Item {
    id: root

    property var model
    property bool grouped: false
    //property alias groupNames: calsDelegateModel.names

    Repeater {
        parent: root.parent // <<< Flow parent is used
//        Layout.fillWidth: true
//        Layout.fillHeight: true

        model: ["normalyze", "automatic", "mechanical"]

//        delegate: Item {
//            ButtonGroup {
//                id: flatButtonGroup
//                //onClicked: if(button.checked) checkState = Qt.Unchecked
//            }
//            FilteredDelegateModel {
//                id: flatFilteredDelegateModel
//                filter: modelData
//                sourceModel: root.model
//                buttonGroup: flatButtonGroup
//            }
//            Repeater {
//                parent: root.parent
//                model: flatFilteredDelegateModel.model
//            }
//            }

        delegate: Component {
            Loader {
                property var modelData: model.modelData
                sourceComponent: grouped ? groupedTypes: flatTypes
            }
        }
    }

    Component {
        id: groupedTypes
        ColumnLayout {
            id: col
            width: 300
            height: 100

            spacing: 0

            FilteredDelegateModel {
                id: filteredDelegateModel
                filter: modelData
                sourceModel: root.model
                buttonGroup: buttonGroup2
            }

            Label {
                id: groupLabel
                text: "<b>" + modelData + "</b>"
                font.capitalization: Font.Capitalize

                Layout.fillWidth: true
            }

            MenuSeparator {
                Layout.fillWidth: true
            }

            RowLayout {
                Layout.fillWidth: true
                Layout.fillHeight: true
                ButtonGroup {
                    id: buttonGroup2
                    //onClicked: if(button.checked) checkState = Qt.Unchecked
                }

                Flow {
                    Repeater {
                        //parent: root.parent
                        model: filteredDelegateModel.model
                    }
                }
            }
        }
    }

    Component {
        id: flatTypes
        Item {
        ButtonGroup {
            id: flatButtonGroup
            //onClicked: if(button.checked) checkState = Qt.Unchecked
        }
        FilteredDelegateModel {
            id: flatFilteredDelegateModel
            filter: modelData
            sourceModel: root.model
            buttonGroup: flatButtonGroup
        }
        Repeater {
            parent: root.parent
            model: flatFilteredDelegateModel.model
        }
        }
    }
}
