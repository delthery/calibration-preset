import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.12
import CalibrationPreset 1.0
import QtQuick.Layouts 1.11
import QtGraphicalEffects 1.15

ApplicationWindow {

    width: Constants.width
    height: Constants.height
    color: Constants.backgroundColor

    Pane {
        id: pane
        anchors.fill: parent
        anchors.rightMargin: 10
        anchors.leftMargin: 10
        anchors.bottomMargin: 10
        anchors.topMargin: 10

        Material.elevation: 5

        ColumnLayout {
            id: columnLayout
            anchors.fill: parent

            ColumnLayout {
                id: headerColumn

                Layout.fillWidth: true
                Layout.alignment: Qt.AlignTop

                RowLayout {
                    Layout.fillWidth: true

                    Label {
                        id: label1
                        text: qsTr("Completed Steps (4 of 27)")
                        Layout.fillWidth: true
                    }

                    RoundButton {
                        id: expandButton
                        flat: true
                        icon.source: "images/Zoom_out.svg"
                    }
                }

                ProgressBar {
                    id: progressBar
                    value: 0.6

                    Layout.fillWidth: true
                }
            }

            Pane {
                id: bodyPane
                Layout.fillHeight: true
                Layout.fillWidth: true
                //Material.elevation: 1
                //Material.background: Material.primary
                clip: false

                GridLayout {
                //Flow {
                    id: bodyRow
                    anchors.fill: parent
                    //flow: width > height ? GridLayout.LeftToRight : GridLayout.TopToBottom
                    flow: height < 200 ? GridLayout.LeftToRight : GridLayout.TopToBottom
                    //flow: GridLayout.TopToBottom

                    Image {
                        id: image

//                        cache: false
//                        smooth: false

                        property int port1: 7
                        property int port2: 11

                        source: svg.value
                        fillMode: Image.PreserveAspectFit

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.preferredWidth: 500
                        Layout.minimumWidth: 300
                        //Layout.preferredHeight: 100
                        Layout.alignment: Qt.AlignRight | Qt.AlignTop
                        //Layout.alignment: Qt.AlignRight | Qt.AlignCenter
                        //                        visible: parent.width > 700 /*(sourceSize.width * 3)*/
                        //                                 && parent.height > sourceSize.height
                        verticalAlignment: Image.AlignTop
                        colorSpace: Material.accent

                        visible: (bodyPane.width > 500 && bodyPane.height > 150)

                        //property alias color: colorOverlay.color
//                            smooth: true

//                            ColorOverlay {
//                                id: colorOverlay
//                                anchors.fill: image
//                                source: image
//                                color: Material.accent//"#000000"
//                            }
                        SvgImage{
                            id: svg
                            path: "images/Calibration/Standards/group_thru_formatted.svg"
                            port1: image.port1
                            port2: image.port2
                        }
                    }

                    Label {
                        id: label
                        text: qsTr("<h4>Step 5 of 27:</h4> Connect the <b>Short Female</b> Standard from Kit <b>85854B</b> to Port 1 of the Calibration Reference Plane | <code>Then Click <b>MEASURE</b></code>")
                        wrapMode: Text.WordWrap
                        textFormat: Text.RichText

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.preferredWidth: 500
                        Layout.minimumWidth: 300
                        //Layout.minimumWidth: parent.width / 3 * 2
                        //width: parent.width / 3 * 2
                        //Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        Layout.alignment: Qt.AlignLeft | Qt.AlignCenter
                        //                    Layout.preferredWidth: 1000
                        //                    Layout.preferredHeight: 100
                    }

                    //                    Connections {
                    //                        target: image
                    //                        Component.onCompleted: visible = Qt.binding(
                    //                                                   function () {
                    //                                                       image.width > 300
                    //                                                   })
                    //                    }
                }
            }


            RowLayout {
                id: bottomRow
                Layout.alignment: Qt.AlignRight | Qt.AlignBottom
                Layout.fillWidth: true


                RoundButton {
                    id: button
                    text: qsTr("PREV")
                    flat: true
                    radius: 16
                    Layout.alignment: Qt.AlignLeft
                    //Layout.preferredWidth: 100

                    Connections {
                        target: button
                        onClicked: {
                            image.port1--// -= 1
                            image.port2--// -= 1
                        }
                    }
                }

                RoundButton {
                    id: button1
                    text: qsTr("MEASURE")
                    highlighted: true
                    flat: true
                    radius: 16
                    //Layout.preferredWidth: 100
                }

                RoundButton {
                    id: button2
                    radius: 16
                    text: qsTr("NEXT")
                    flat: true
                    enabled: !roundButton.checked
                    //Layout.preferredWidth: 100
                    Connections {
                        target: button2
                        onClicked: {
                            image.port1 += 1
                            image.port2 += 1
                        }
                    }
                }

                RoundButton {
                    id: roundButton
                    text: "+"
                    checkable: true
                    enabled: true
                    hoverEnabled: true
                    display: AbstractButton.IconOnly
                    flat: true
                    icon.source: "images/run.svg"
                    highlighted: checked
                }
            }

        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:1.33;height:480;width:640}D{i:1}
}
##^##*/

