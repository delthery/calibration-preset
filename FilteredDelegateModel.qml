import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import QtQml.Models 2.15


Item {
    id: root
    property var filter
    property var sourceModel
    property ButtonGroup buttonGroup
    property alias model: delegateModel

    DelegateModel {
        id: delegateModel
        model: sourceModel

        filterOnGroup: filter
        items.includeByDefault: false
        persistedItems.includeByDefault: false

        groups: [
            DelegateModelGroup { name: filter; includeByDefault: false }
        ]

        delegate: RoundButton {
            id: calibrationTypePane2
            text: model.name
            checkable: true
            width: 100
            icon.source: checked ? "images/Check/Check.svg" : ""
            Material.elevation: hovered ? 10 : 1
            hoverEnabled: true
            ButtonGroup.group: buttonGroup
            flat: (buttonGroup.checkState != Qt.Unchecked ) && !checked

            property bool wasChecked: false

            Connections {
                target: calibrationTypePane2
                onPressed: { wasChecked = checked }
                onReleased: {
                    if(wasChecked) {
                        checked = false;
                        toggled();
                    }
                }
            }
        }
        Component.onCompleted: {
            var rowCount = model.count
            //calsDelegateModel2.items.remove(0, rowCount)
            for (var i = 0; i < rowCount; i++) {
                var entry = model.get(i)
                if(entry.category === filter)
                    delegateModel.items.insert(entry, entry.category);
            }
        }
    }
}
