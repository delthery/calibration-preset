import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Material.impl 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.15
import QtQml.Models 2.12
import Qt.labs.qmlmodels 1.0


Pane {
    id: root

    property SwipeView swipe
    property alias portsModel: presetConfigPorts.portsModel
    property alias groupsModel: presetConfigPorts.groupsModel
    property alias activeGroup: presetConfigPorts.activeGroup
    property alias activeGroupOut: presetConfigPorts.activeGroupOut

//    Rectangle {
//        id: testRectangle1
//        anchors.fill: parent
//        color: Material.primary
//    }

//    Rectangle {
//        id: testRectangle2
//        anchors.fill: scrollView
//        color: "transparent"
//        border.color: Material.color(Material.Red)
//    }

    Stepper {
        id: stepper
        anchors.fill: parent

        Step {
            index: "1"
            name: "Ports To Be Calibrated"
            description: "Select the Ports That Need to Be Calibrated by Clicking on Them Below:"
            content: PresetConfigPorts{
                id: presetConfigPorts
                activeGroup: tabBarH.currentIndex + 1
            }
            actions: [
                Button {
                    text: "Back"
                    flat: true
                    enabled: false

                    onClicked: stepper.prev()
                },
                Button {
                    text: "Next"
                    highlighted: true
                    Material.accent: Material.primary

                    onClicked: stepper.next()
                },
                Button {
                    id: selectAllButon
                    //flat: true
                    highlighted: true
                    Material.accent: Material.primary
                    text: "Select All Available"
                    icon.source: "images/select_all.svg"

                    onClicked: presetConfigPorts.selectAllPorts()
                },
                Button {
                    id: unselectButton
                    //flat: true
                    highlighted: true
                    Material.accent: Material.primary
                    text: "Unselect"

                    onClicked: presetConfigPorts.unselectPorts()
                }
            ]
        }

        Step {
            index: "2"
            name: "Type of Calibration"
            description: "Select the Calibration <b>TYPE</b> and <b>ALGORITHM</b> Below:"

            content: PresetConfigModels {
                grouped: groupedModeCheckbox.checked
            }

            actions: [
                Button {
                    text: "Back"
                    flat: true

                    onClicked: stepper.prev()
                },
                Button {
                    text: "Next"
                    highlighted: true
                    Material.accent: Material.primary

                    onClicked: stepper.next()
                },
                Button {
                    id: autoTypeSelectionButton
                    //flat: true
                    highlighted: true
                    Material.accent: Material.primary
                    text: "Match the best option"

                    icon.source: "images/Calibration/Calibrate.svg"
                },
                Switch {
                    id: groupedModeCheckbox
                    text: "GROUP BY TYPE"
                }

            ]
        }

        Step {
            index: "3"
            name: "Calibration Plane"
            description: "Yet Another Description"
            content: Pane {

                Material.background: Material.primary

//                TableView {
//                    anchors.fill: parent

//                    columnSpacing: 10
//                    rowSpacing: 10
//                    boundsBehavior: Flickable.StopAtBounds

//                    model: TableModel {
//                        TableModelColumn { display: "checked" }
//                        TableModelColumn { display: "amount" }
//                        TableModelColumn { display: "fruitType" }
//                        TableModelColumn { display: "fruitName" }
//                        TableModelColumn { display: "fruitPrice" }

//                        // Each row is one type of fruit that can be ordered
//                        rows: [
//                            {
//                                // Each property is one cell/column.
//                                checked: false,
//                                amount: 1,
//                                fruitType: "Apple",
//                                fruitName: "Granny Smith",
//                                fruitPrice: 1.50
//                            },
//                            {
//                                checked: true,
//                                amount: 4,
//                                fruitType: "Orange",
//                                fruitName: "Navel",
//                                fruitPrice: 2.50
//                            },
//                            {
//                                checked: false,
//                                amount: 1,
//                                fruitType: "Banana",
//                                fruitName: "Cavendish",
//                                fruitPrice: 3.50
//                            }
//                        ]
//                    }
//                    delegate:  TextInput {
//                        text: model.display
//                        padding: 12
//                        selectByMouse: true

//                        onAccepted: model.display = text

//                        Rectangle {
//                            anchors.fill: parent
//                            color: "#efefef"
//                            z: -1
//                        }
//                    }
//                }


            }
            actions: [
                Button {
                    text: "Back"
                    flat: true

                    onClicked: stepper.prev()
                },
                Button {
                    text: "Finish"
                    highlighted: true
                    Material.accent: Material.primary
                    onClicked: stepper.next()
                },
                Button {
                    text: "Advanced Settings"
                    highlighted: true
                    Material.accent: Material.primary
                }
            ]
        }
    }

    /*
    GridLayout {
        id: grid

        // anchors.fill: parent

        // FIRST COLUMN ////////////////////////////

        StepIndicator {
            id: firtsStepIndicator
            text: "1"
            Layout.column: 0
            Layout.row: 0
        }

        VerticalLine {
            Layout.column: 0
            Layout.row: 1
        }

        StepIndicator {
            id: secondStepIndicator
            text: "2"

            Layout.column: 0
            Layout.row: 2
        }

        VerticalLine {
            Layout.column: 0
            Layout.row: 3
        }

        StepIndicator {
            id: thirdStepIndicator
            text: "3"
            Layout.column: 0
            Layout.row: 4
        }

        // SECOND COLUMN////////////////////////////

        StepLabel {
            Layout.column: 1
            Layout.row: 0
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop

            name: "Ports To Be Calibrated"
            description: "Select the Ports That Need to Be Calibrated by Clicking on Them Below:"
        }

        StepBody {
            Layout.column: 1
            Layout.row: 1
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

            PresetConfigPorts {
                id: portsFlow
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
            actions: [
                Button {
                    id: selectAllButon
                    flat: true
                    highlighted: true
                    text: "Select All Available"

                    icon.source: "images/select_all.svg"

                    Connections {
                        target: selectAllButon
                        onClicked: {
                            var model = portsModel
                            for (var i = 0; i < model.rowCount(); i++) {
                                if (!model.get(i).used)
                                    portsRepeater.itemAt(
                                                i).checked = true
                            }
                        }
                    }
                },
                Button {
                    id: unselectButton
                    flat: true
                    text: "Unselect"
                    Connections {
                        target: unselectButton
                        onClicked: {
                            for (var i = 0; i < portsRepeater.count; i++) {
                                portsRepeater.itemAt(i).checked = false
                            }
                        }
                    }
                }
            ]
        }

        StepLabel {
            Layout.column: 1
            Layout.row: 2
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop

            name: "Type of Calibration"
            description: "Select the Calibration <b>TYPE</b> and <b>ALGORITHM</b> Below:"
        }

        ColumnLayout {
            Layout.column: 1
            Layout.row: 3

            Pane {
                id: calsPane

                leftPadding: 40
                rightPadding: 40
                //Material.elevation: 5

                ColumnLayout {
                    anchors.fill: parent

                    CheckBox {
                        id: groupedMode
                        text: "Group Types"
                    }

                    PresetConfigModels {
                        id: flow2
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                    }
                }
            }
            RowLayout {
                Button {
                    id: autoTypeSelectionButton
                    flat: true
                    highlighted: true
                    text: "Match the best option"

                    icon.source: "images/Calibration/Calibrate.svg"
                }
                Button {
                    id: secondStepNextButton
                    flat: true
                    text: "Next"
                }
            }
        }

        StepLabel {
            Layout.column: 1
            Layout.row: 4
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop

            name: "Calibration Plane"
            description: "Select the Calibration Plane <b>TYPE</b> and <b>ALGORITHM</b> Below:"
        }

        StepBody {
            Layout.column: 1
            Layout.row: 5
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

            TableView {
                anchors.fill: parent

                columnSpacing: 10
                  rowSpacing: 10
                  boundsBehavior: Flickable.StopAtBounds

                  model: TableModel {
                      TableModelColumn { display: "checked" }
                      TableModelColumn { display: "amount" }
                      TableModelColumn { display: "fruitType" }
                      TableModelColumn { display: "fruitName" }
                      TableModelColumn { display: "fruitPrice" }

                      // Each row is one type of fruit that can be ordered
                      rows: [
                          {
                              // Each property is one cell/column.
                              checked: false,
                              amount: 1,
                              fruitType: "Apple",
                              fruitName: "Granny Smith",
                              fruitPrice: 1.50
                          },
                          {
                              checked: true,
                              amount: 4,
                              fruitType: "Orange",
                              fruitName: "Navel",
                              fruitPrice: 2.50
                          },
                          {
                              checked: false,
                              amount: 1,
                              fruitType: "Banana",
                              fruitName: "Cavendish",
                              fruitPrice: 3.50
                          }
                      ]
                  }
                  delegate:  TextInput {
                      text: model.display
                      padding: 12
                      selectByMouse: true

                      onAccepted: model.display = text

                      Rectangle {
                          anchors.fill: parent
                          color: "#efefef"
                          z: -1
                      }
                  }
            }

            actions: [
                Button {
                    text: "Back"
                    flat: true
                },
                Button {
                    text: "Finish"
                    flat: true
                    highlighted: true
                }
            ]
        }
    }
*/

    /*
     *  Components
     */

//    component VerticalLine: ToolSeparator {
//            Layout.fillHeight: true
//            Layout.alignment: Qt.AlignCenter
//    }

//    component StepIndicator: RoundButton {
//        property int state: PresetConfig.StepIndicatorState.Disabled
//        Material.elevation: 1
//    }

//    component StepLabel: Label {
//        id: stepLabel
//        property string name
//        property string description

//        property string message: "<b>%1<b/><br><code><font color=\"%3\">%2</font></code>"

//        text: message.arg(name).arg(description).arg(Material.secondaryTextColor)
//        padding: 10

//        verticalAlignment: Text.AlignVCenter
//        wrapMode: Text.WordWrap
//        textFormat: Text.RichText
//    }

//    component StepBody: ColumnLayout {
//        default property Item body
//        property list<Item> actions

//        Pane {
//            id: stebBodyPane

//            //Layout.fillHeight: false
//            Layout.fillWidth: true
//            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

//            leftPadding: 40
//            rightPadding: 40

//            //Material.elevation: 5
//            ColumnLayout {
//                anchors.fill: parent
//                data: body
//            }
//        }

//        RowLayout {
//            id: actionsRow
//            Layout.fillWidth: true
//            //Layout.alignment: Qt.AlignHRight | Qt.AlignTop
//            height: swipe.currentIndex == 1 ? 0 : undefined
//            visible: swipe.currentIndex == 1 ? false : true

//            data: actions
//        }
//    }

//    component ConfigStep: ColumnLayout {
//        property alias indicator: indicator
//        property alias label: label
//        property alias content: content
//        RowLayout {
//            StepIndicator{ id: indicator }
//            StepLabel{ id: label; Layout.fillWidth: true }
//        }
//        RowLayout {
//            VerticalLine {}
//            StepBody { id: content; Layout.fillWidth: true }
//        }
//    }
}
