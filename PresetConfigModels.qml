import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Material.impl 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.15
import QtQml.Models 2.12
import Qt.labs.qmlmodels 1.0

Flow {
    //RowLayout {
    id: root

    property bool grouped: false
    //anchors.fill: parent
    spacing: 10

    CalModels {
        model: calsModel
        grouped: root.grouped
    }
}
