import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Material.impl 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.15
import QtQml.Models 2.12
import Qt.labs.qmlmodels 1.0

Flow {
    id: portsFlow

    property ListModel portsModel
    property ListModel groupsModel
    //property alias activeGroup : portsFlow._activeGroup
    signal selectAllPorts
    signal unselectPorts

    property int activeGroup //: activeGroupOut
    property int activeGroupOut

    spacing: 10

    onActiveGroupChanged: {
        console.log("ACTIVE GROUP:" + activeGroup);
        activeGroupOut = activeGroup
    }
    onActiveGroupOutChanged: console.log("ACTIVE GROUP OUT:" + activeGroupOut)

    Repeater {
        id: portsRepeater
        model: portsModel
        delegate: RoundButton {
            id: portButton

            property bool inActiveGroup: model.used && model.group === activeGroup
            property bool highlightedMode: highlightedInGroup && !inActiveGroup
            property int portIndex: index + 1
            //property int _activeGroup: portsFlow.activeGroup

            width: 46
            height: 46
            text: portIndex
            checkable: !used || inActiveGroup
            checked: inActiveGroup
            highlighted: highlightedMode
            flat: used && !inActiveGroup
            hoverEnabled: true

            Material.elevation: hovered ? 10 : 3

            layer.enabled: highlightedMode
            layer.effect: Glow {
                samples: 50
                radius: 27
                color: Material.accent
                transparentBorder: true
            }


            ToolTip {
                id: tooltip

                property bool usedPort: model.used
                property var group: usedPort ? groupsModel[model.group] : -1
                readonly property string usedPortModelsTooltip: "<br> <style type=\"text/css\">
                                                                .tg  {border-collapse:collapse;border-spacing:0;}
                                                                .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
                                                                overflow:hidden;padding:10px 5px;word-break:normal;}
                                                                .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
                                                                font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
                                                                .tg .tg-1{background-color:#9b9b9b;border-color:#efefef;text-align:center;vertical-align:top}
                                                                .tg .tg-2{background-color:#9b9b9b;text-align:center;vertical-align:top}
                                                                </style>
                                                                <table class=\"tg\">
                                                                <thead>
                                                                <tr>"

                delay: 300
                timeout: 5000
                visible: hovered && !(portButton.inActiveGroup)
                contentItem: Label {
                    text: tooltip.text
                    font: tooltip.font
                    textFormat: Text.RichText
                    wrapMode: Text.Wrap
                }

                function updateTooltipText() {
                    var usedPort = model.used
                    var groupItem = usedPort ? portsFlow.groupsModel.get(model.group) : -1
                    var groupName = usedPort ? groupItem.name : ""


                    var text = usedPort ? "<h3><img src = \"images/port_group.svg\"> " + groupName + "</h3> <br><tt>Press to select group</tt> " :
                                          "<h3><img src = \"images/Calibration/port-m.svg\" width=\"24\" height=\"24\"> Uncalibrated port</h3> <br> <tt>Press to select for calibration</tt>"
                    if(usedPort && groupItem.calibration.count > 0) {
                        text += usedPortModelsTooltip
                        for (var i = 0; i < groupItem.calibration.count; i++) {
                            text += "<th class=\"tg-" + (i + 1) + "\">" + groupItem.calibration.get(i).type + "</th>"
                        }
                        text += "</tr></thead></table>"
                    }
                    tooltip.text = text
                }

                onAboutToShow: updateTooltipText()
                Component.onCompleted: updateTooltipText()
            }

            onHoveredChanged: {
                if (used) {
                    var model = portsModel
                    for (var i = 0; i < model.rowCount(); i++) {
                        var item = model.get(i)
                        if (item.used && item.group === group) {
                            item.highlightedInGroup = portButton.hovered
                        }
                     }
                }
            }
            onReleased: {
                var modelList = portsModel
                var modelItem = modelList.get(portIndex - 1)

                if (used) {
                    console.log("Press on USED port (in active group = " + inActiveGroup + ")")
                    console.log("Checkable=" + checkable)
                    if (!inActiveGroup) { // click on used port in another group
                        console.log("Swith to another group "+modelItem.group + " activeGroupOut: " +portsFlow.activeGroupOut)
                        portsFlow.activeGroupOut = modelItem.group //FIXME << break binding

                        //portsFlow.activeGroup(modelItem.group)

                        for (var i = 0; i < modelList.rowCount(); i++) {
                            modelList.get(i).highlightedInGroup = false
                        }

                    } else { // click on used port in currently slected group
                        console.log("Remove from group")
                        modelItem.used = false
                        modelItem.group = -1 // undefined
                        modelItem.highlightedInGroup = false
                    }
                } else { // ckick on unused port
                    console.log("Press on UNUSED port (group = " + activeGroup + ")")
                    console.log("Add to group")
                    modelItem.used = true
                    modelItem.group = activeGroup
                }
            }
        }
    }

    onSelectAllPorts: {
        var model = portsModel
        for (var i = 0; i < model.rowCount(); i++) {
            var item = model.get(i)
            if (!item.used) {
                item.used = true
                item.group = activeGroup
            }
        }
    }

    onUnselectPorts: {
        var model = portsModel
        for (var i = 0; i < model.rowCount(); i++) {
            var item = model.get(i)
            if(item.used && item.group === activeGroup) {
                item.used = false
                item.group = -1
            }

        }
    }
    Component.onCompleted: activeGroupOut = activeGroup

}
