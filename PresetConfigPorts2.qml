import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Material.impl 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.15
import QtQml.Models 2.12
import Qt.labs.qmlmodels 1.0

Flow {

    Loader {
        property int count: 16              // total count of source elements
        property int level: 0              // nesting level 0 - first level log2(count)-1 - last level
        property int index: 0              // index of element

        sourceComponent: flow
    }
    Loader {
        property int count: 16              // total count of source elements
        property int level: 0              // nesting level 0 - first level log2(count)-1 - last level
        property int index: 1              // index of element

        sourceComponent : flow
    }

//    PairRepeater{
//        count: 16
//        level: 0
//        index: 0
//    }

//    PairRepeater{
//        count: 16
//        level: 0
//        index: 1
//    }

    component PairRepeater: Flow {
        id: flow
//        property int count              // total count of source elements
//        property int level              // nesting level 0 - first level log2(count)-1 - last level
//        property int index              // index of element

        Loader {
            property int count: count              // total count of source elements
            property int level: level+1              // nesting level 0 - first level log2(count)-1 - last level
            property int index: index              // index of element

            sourceComponent: (level === Math.log2(count)-1) ? button : flow
        }
        Loader {
            property int count: count              // total count of source elements
            property int level: level+1              // nesting level 0 - first level log2(count)-1 - last level
            property int index: index+1              // index of element

            sourceComponent: (level === Math.log2(count)-1) ? button : repeater
        }

    }

    Component {
        id: button

        RoundButton{
            text: index
        }
    }
    Component {
        id: repeater
        PairRepeater{ /*count: count; level: level; index: index*/}
    }
}
