import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Material.impl 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.15

Pane {
    id: groupsPane
    Material.elevation: 5

    ColumnLayout {
        anchors.fill: parent

//                        Image {
//                            source: "images/Graphics/Graphics.svg"
//                            Layout.alignment: Qt.AlignCenter
//                        }

        //SwipeView {
        Flow {
        //ColumnLayout{
            spacing: 10
            Layout.leftMargin: 40
            Layout.rightMargin: 40

            id: groupsSwipe
            Layout.fillHeight: true
            Layout.fillWidth: true

            //currentIndex: tabBarH.currentIndex
            Repeater {
                model: groupsModel1

                delegate: Pane {
                    id: pane

                    //anchors.fill: parent
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom

                    leftPadding: 30
                    rightPadding: 30

                    Material.elevation: 10
                    Material.background: Material.dialogColor//Material.primary
                    ColumnLayout {
                        anchors.fill: parent

                        RowLayout {
//                                            Image {
//                                                source: "images/Calibration/portsicon.svg"
//                                                width: 16
//                                                height:16
//                                                Layout.alignment: Qt.AlignHCenter// | Qt.AlignBottom
//                                            }

                            Label {
                                text: "<h3><img src=\"images/Calibration/portsicon.svg\" align=\"middle\" width=\"16\" height=\"16\"> " + name + "</h3>"
                                //text: "<h3> <img src=\"images/Add.svg\" width=\"24\" height=\"24\"> " + name + "</h3>"
                                Layout.fillWidth: true
                                Layout.alignment: Qt.AlignHCenter// | Qt.AlignBottom
                                verticalAlignment: Text.AlignVCenter
                                wrapMode: Text.WordWrap
                                textFormat: Text.RichText
                                font.capitalization: Font.AllUppercase
                            }
                        }

                        RowLayout {
                            //spacing: -10
                            Repeater {
                                model: ports
                                RoundButton {
                                    text: port
                                    Material.elevation: 1
                                    Material.background: Material.background//backgroundDimColor
                                }
                            }
                        }

                        RowLayout {
                            spacing: 5
                            Repeater {
                                model: calibration
                                Button {
                                    id: control
                                    width: 100
                                    text: type
                                    //radius: 20
                                    Material.elevation: 1
                                    //enabled: false
                                    //highlighted: true

                                    background: Rectangle {
                                            color: control.Material.primary//backgroundColor
                                            radius: 20//control.Material.elevation > 0 ? control.radius : 0

                                            layer.enabled: control.enabled && control.Material.elevation > 0
                                            layer.effect: ElevationEffect {
                                                elevation: control.Material.elevation
                                            }
                                        }
                                }
                            }
                        }
                    }
                }
            }
        }

//                        RowLayout {
//                            Layout.fillWidth: true

//                            Button {
//                                Layout.preferredWidth: 200
//                                Layout.preferredHeight: 70
//                                highlighted: false
//                                Material.background: Material.primary
//                                text: !hovered ? "Calibrate" : "Calibrate<br><code>6 ports</code>"
//                            }
//                            CheckBox {
//                                text: "All channels"
//                            }
//                        }
    }
}
