import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import QtQml 2.12
import QtGraphicalEffects 1.0

ColumnLayout {
    spacing: -1
    clip: true

    Pane {
        z: 1
        Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
        Layout.fillWidth: true
        Material.elevation: 10
        Material.background: Material.primary
        height: 200
        bottomPadding: 0

        RowLayout {
            id: rowLayout
            width: 100
            height: 100
            anchors.fill: parent
            spacing: 10

//                    Image {
//                        source: "images/Calibrate.svg"
//                        fillMode: Image.PreserveAspectFit
//                        height: 48
//                        width: 48
//                    }

            Label {
                id: label
                text: qsTr("Presets")
                Layout.fillWidth: true
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
                font.weight: Font.ExtraLight
                font.capitalization: Font.MixedCase
                font.pointSize: 14

                //Material.foreground: Material.textSelectionColor
            }

            RoundButton {
                id: addRoundButton
                icon.source: "images/Add.svg"
                flat: true

                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }

            RoundButton {
                padding: 0
                id: moreRoundButton
                //text: "..."
                icon.source: "images/More.svg"
                flat: true

                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

                ButtonGroup { id: buttonGroup; }

                Menu {
                    id: menu
                    y: parent.height
//                    MenuItem {
//                        text: "New..."
//                        icon.source: "images/Add.svg"
//                    }
                    MenuItem {
                        text: "Load..."
                        icon.source: "images/Load.svg"
                        ButtonGroup.group: buttonGroup
                    }
                    MenuItem {
                        text: "Save All..."
                        icon.source: "images/Save.svg"
                        ButtonGroup.group: buttonGroup
                    }
                    MenuSeparator{}
                    MenuItem {
                        id: showRecentlyUsed
                        text: "Show recently used"
                        checkable: true
                        //icon.source: checked ? "images/Add.svg" : ""
                    }
                    MenuSeparator{}

                    Menu {
                        title: "Sorting"
                        //icon.source: "images/"

                    MenuItem {
                    //RadioButton {
                        text: "Sort by name"
                        checkable: true
                        //icon.source: checked ? "images/Add.svg" : ""
                        ButtonGroup.group: buttonGroup
                    }
                    MenuItem {
                    //RadioButton {
                        text: "Sort by date"
                        checkable: true
                        //icon.source: checked ? "images/Add.svg" : ""
                        ButtonGroup.group: buttonGroup
                    }
                    }
                    MenuSeparator{}
                    MenuItem {
                        text: "Delete"
                        icon.source: "images/Delete.svg"
                        ButtonGroup.group: buttonGroup
                    }
                }

                Connections {
                    target: moreRoundButton
                    onClicked: menu.open()//popup()
                }
            }
        }

    }

    DropShadow {
            anchors.fill: rect
            cached: true
            horizontalOffset: 3
            verticalOffset: 3
            radius: 4.0
            samples: 16
            color: "#80000000"
            source: pane
        }

    Pane {
        id: pane
        Layout.fillWidth: true
        Layout.fillHeight: true

        padding: 0

        ColumnLayout {
            anchors.fill: parent

            ListView {
                id: listView
                Layout.bottomMargin: 10
                Layout.topMargin: 10

                //spacing: 10

                Layout.fillHeight: true
                Layout.fillWidth: true

                model: ListModel {
                    ListElement {
                        name: "Preset 1"
                        group: "Recently Used"
                    }

                    ListElement {
                        name: "Preset 2"
                    }

                    ListElement {
                        name: "Long preset name"
                        running: true
                    }

                    ListElement {
                        name: "Very long preset name created by user"
                    }
                }
                ButtonGroup {
                    id: idBg
                }

                delegate: ItemDelegate {
                    id: control
                    clip: true
                    text: "<b>" + name + "</b><br><i>" + Qt.formatDateTime(new Date(), "yyyy.MM.dd")+"</i>"
                    icon.source: model.running ? "images/Play.svg": (model.editing ? "images/edit-icon.svg" : "images/port_group.svg")
                    icon.color: model.running ? Material.accent : Material.iconColor
                    highlighted: checked//model.running
                    Material.foreground: model.running ? Material.accent : undefined
                    //Material.background: Material.primary
                    anchors.left: parent.left
                    anchors.right: parent.right
                    checkable: true
                    checked: model.group === "Recently Used"
                    ButtonGroup.group: idBg

                    MouseArea {
                        parent: control.parent
                        anchors.fill: control//parent
                        propagateComposedEvents: true
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        onClicked: {
                            if (mouse.button == Qt.RightButton)
                                itemMenu.popup()
                            mouse.accepted = false
                        }
                    }

                    Menu {
                        id: itemMenu
                        MenuItem{
                            text:  model.running ? "Stop" : "Calibrate"
                            icon.source: model.running ? "images/block.svg" : "images/play.svg"
                        }
                        MenuItem {
                            text: "Edit"
                            icon.source: "images/edit-icon.svg"
                            visible: !model.running
                            height: model.running ? 0 : undefined
                        }

                        MenuItem {
                            text: "Save As..."
                            icon.source: "images/Save.svg"
                        }
                        MenuSeparator{}
                        MenuItem {
                            text: model.running ? "Stop and Delete" : "Delete"
                            icon.source: "images/Delete.svg"
                            ButtonGroup.group: buttonGroup
                        }
                    }

                    //onPressed: itemMenu.show()

                    Connections {
                        target: control
                        onPressed: {
                            if (control.pressedButtons & Qt.RightButton)
                               itemMenu.popup()
                        }
                    }
                }

                section.property: showRecentlyUsed.checked ? "group" : ""
                section.delegate: ColumnLayout {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    MenuSeparator {
                        id: sep
                        Layout.fillWidth: true
                        visible: section == ""
                        leftPadding: 10
                        rightPadding: 10
                    }

                    Label {
                        text: section
                        color: Material.accent
                        visible: section !== ""
                        leftPadding: 10
                    }
                }
            }
        }
    }

//            RoundButton {
//                text: "+"
//                highlighted: true
//                Layout.alignment: Qt.AlignRight
//                Material.elevation: 6
//            }
}


