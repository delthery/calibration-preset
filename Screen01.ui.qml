import QtQuick 2.15
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Material.impl 2.12
import QtQuick.Studio.Effects 1.0
import QtQml.Models 2.15
import QtGraphicalEffects 1.15


Rectangle {
    id: root
    anchors.fill: parent
    width: 1280 //Constants.width
    height: 720 //Constants.height

    color: "#FF303030"//Constants.backgroundColor

    SplitView {
        orientation: Qt.Horizontal
        anchors.fill: parent
        //Material.background: Material.primary

        handle: Rectangle {
                implicitWidth: 4
                implicitHeight: 4
                color: SplitHandle.pressed ? Material.primary
                    : (SplitHandle.hovered ? Qt.lighter("#c2f4c6", 1.1) : Material.dividerColor)
            }

        PresetSidebar {
            id: sidebar

            SplitView.maximumWidth: 600
            SplitView.minimumWidth: 200
            SplitView.preferredWidth: 300
        }

    Item {
        id: item1

        SplitView.minimumWidth: 100
        SplitView.fillWidth: true

        //anchors.fill: parent
        Rectangle {
            id: rectangle1
            color: "#FF303030"//"#373333"
            anchors.fill: parent
        }

        ListModel {
            id: portsModel
            ListElement {
                port: 1
                used: false
            }
            ListElement {
                port: 2
                used: false
            }
            ListElement {
                port: 3
                used: false
            }
            ListElement {
                port: 4
                used: false
            }
            ListElement {
                port: 5
                used: false
            }
            ListElement {
                port: 6
                used: false
            }
            ListElement {
                port: 7
                used: true
                group: 0
            }
            ListElement {
                port: 8
                used: true
                group: 0
            }
            ListElement {
                port: 9
                used: true
                group: 0
            }
            ListElement {
                port: 10
                used: true
                group: 0
            }
            ListElement {
                port: 11
                used: true
                group: 1
            }
            ListElement {
                port: 12
                used: true
                group: 1
            }
            ListElement {
                port: 13
                used: true
                group: 2
            }
            ListElement {
                port: 14
                used: true
                group: 2
            }
            ListElement {
                port: 15
                used: true
                group: 2
            }
            ListElement {
                port: 16
                used: true
                group: 2
                highlightedInGroup: false
            }
        }

        ListModel {
            id: groupsModel1

            ListElement {
                name: "Group1"
                calibration: [
                    ListElement {
                        type: "TRL"
                    },
                    ListElement {
                        type: "Power"
                    }
                ]
                ports: [
                    ListElement {
                        port: 7
                    },
                    ListElement {
                        port: 8
                    },
                    ListElement {
                        port: 9
                    },
                    ListElement {
                        port: 10
                    }
                ]
            }

            ListElement {
                name: "Group2"
                calibration: [
                    ListElement {
                        type: "SOLT"
                    }
                ]
                ports: [
                    ListElement {
                        port: 11
                    },
                    ListElement {
                        port: 12
                    }
                ]
            }

            ListElement {
                name: "Group3"
                calibration: [
                    ListElement {
                        type: "ACM"
                    },
                    ListElement {
                        type: "Power"
                    }
                ]
                ports: [
                    ListElement {
                        port: 13
                    },
                    ListElement {
                        port: 14
                    },
                    ListElement {
                        port: 15
                    },
                    ListElement {
                        port: 16
                    }
                ]
            }
        }

        ListModel {
            id: calsModel
            ListElement {
                name: "ACM"
                category: "automatic"
                ports: [
                    /*1, 2, 3, 4 ]*/
                    ListElement {
                        port: 1
                    },
                    ListElement {
                        port: 2
                    },
                    ListElement {
                        port: 3
                    },
                    ListElement {
                        port: 4
                    }
                ]
            }
            ListElement {
                name: "SOLT"
                category: "mechanical"
                ports: [
                    ListElement {
                        port: 1
                    },
                    ListElement {
                        port: 2
                    }
                ]
            }
            ListElement {
                name: "TRL"
                category: "mechanical"
                ports: [
                    ListElement {
                        port: 1
                    },
                    ListElement {
                        port: 2
                    }
                ]
            }
            ListElement {
                name: "Open"
                category: "normalyze"
                ports: [
                    ListElement {
                        port: 1
                    }
                ]
            }
            ListElement {
                name: "Short"
                category: "normalyze"
                ports: [
                    ListElement {
                        port: 1
                    }
                ]
            }
            ListElement {
                name: "Thru"
                category: "normalyze"
                ports: [
                    ListElement {
                        port: 1
                    },
                    ListElement {
                        port: 2
                    }
                ]
            }
        }


        ColumnLayout {
            anchors.fill: parent
            spacing: 0

            Pane {
                id: presetPane
                property var bg: Material.background
                property bool editMode: swipe.currentIndex == 0

                Material.background: bg

                height: 200
                bottomPadding: 0//groupTabBarH.visible ? 0 : 12
                Layout.fillHeight: false
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                Material.elevation: editMode ? 5 : 0
                z: 1000

                //Material.background: swipe.currentIndex == 0 ? Material.primary : undefined
                ColumnLayout {
                    anchors.fill: parent

                    RowLayout {
                        Layout.fillWidth: true

                        //flow: Flow.TopToBottom
                        //layoutDirection: "RightToLeft"

                        RowLayout {
                            id: presetHeaderTextWithIcon
                            Image {
                                source: "images/port_group.svg"
                                Layout.leftMargin: 20
                            }

                            Label {
                                id: presetHeaderLabel
                                //text: "<h2><img src=\"images/Calibration/portsicon.svg\"> Calibration Preset</h2>"

                                //text: "<h2>Calibration Preset</h2>"
                                //textFormat: Text.RichText
                                //wrapMode: Text.Wrap

                                text: "Preset 1"
                                elide: Text.ElideRight
                                verticalAlignment: Text.AlignVCenter
                                font.weight: Font.ExtraLight
                                font.capitalization: Font.MixedCase
                                font.pointSize: 14

                                Layout.fillWidth: true
                            }
                        }

                        RowLayout {
                            id: tabbarFirstPlace

                            RowLayout {
                                id: groupTabBarH
                                parent: root.width < 800 ? tabbarSecondPlace : tabbarFirstPlace

                                height: swipe.currentIndex == 1 ? 0 : undefined
                                visible: swipe.currentIndex == 1 ? false : true

                                //anchors.fill: parent
                                Layout.fillWidth: true

                                //visible: swipe.currentIndex == 1 ? true : false
                                //height: swipe.currentIndex == 1 ? undefined : 0

                                TabBar {
                                    id: tabBarH
                                    position: TabBar.Header

                                    Layout.alignment: Qt.AlignHLeft | Qt.AlignVCenter
                                    Layout.minimumWidth: 300
                                    Layout.maximumWidth: 700

                                    currentIndex: presetConfigPage.activeGroupOut

                                    //parent: root.width < 800 ? tabbarSecondSpace : groupTabBarH

                                    //Layout.fillWidth: true
                                    Repeater {
                                        model: groupsModel1
                                        delegate: TabButton {
                                            id: tabButtonH
                                            text: name
                                            //icon.source: checked ? "images/port_group.svg" : ""
                                            icon.source: checked ? "images/Calibration/ports.svg" : ""
                                            icon.height: 16
                                            icon.width: 16

                                            Material.primary: checked ? Material.Red : undefined

                                            Layout.preferredWidth: 350
                                        }
                                    }

                                    //                            TabButton {
                                    //                                text: "New Group"
                                    //                                Material.accent: Material.red
                                    //                                visible: false
                                    //                                enabled: false

                                    //                                width: visible ? undefined : 0
                                    //                            }
                                }

                                RoundButton {
                                    id: addGroupButtonH
                                    //width: 56
                                    height: 56
                                    icon.source: "images/Add.svg"

                                    //parent: root.width < 800 ? tabbarSecondSpace : groupTabBarH
                                    //text: "Save  "
                                    //anchors.left: parent.left
                                    //anchors.bottom: parent.bottom
                                    //Layout.fillWidth: true
                                    Layout.alignment: Qt.AlignHRight | Qt.AlignTop

                                    //anchors.leftMargin: 30
                                    //anchors.bottomMargin: 84
                                    //highlighted: true
                                    //Material.background: Material.accent
                                    Material.elevation: hovered ? 6 : 1

                                    flat: true
                                    //highlighted: true
                                    onClicked: swipe.currentIndex = 0

                                    ToolTip.text: "Calibrate more ports"
                                    ToolTip.delay: 1000
                                    ToolTip.timeout: 5000
                                    ToolTip.visible: hovered
                                }
                            }
                        }

                        RowLayout {
                            id: presetActionBar
                            RoundButton {
                                id: runPresetButton
                                //width: 56
                                height: 56
                                width: 100
                                icon.source: "images/play.svg"
                                icon.height: 16
                                icon.width: 16
                                //text: hovered ? "Calibrate " : ""
                                visible: root.state == "PreviewPresetState"
                                //checkable: true

                                //onClicked: swipe.currentIndex = (checked ? 1 : 0)

                                //anchors.left: parent.left
                                //anchors.bottom: parent.bottom
                                //Layout.fillWidth: true
                                Layout.alignment: Qt.AlignHRight | Qt.AlignTop

                                //anchors.leftMargin: 30
                                //anchors.bottomMargin: 84
                                flat: true
                                //highlighted: true//hovered
                                //Material.background: Material.accent
                                Material.elevation: hovered ? 6 : 0

                                ToolTip.text: "Start calibration"
                                ToolTip.delay: 1000
                                ToolTip.timeout: 5000
                                ToolTip.visible: hovered

                                Connections {
                                    target: runPresetButton
                                    onClicked: {
                                        var component = Qt.createComponent("CalibrationStep.qml")
                                        var window    = component.createObject(root)
                                        window.show()
                                    }
                                }
                            }

                            RoundButton {
                                id: editPresetButton
                                //width: 56
                                height: 56
                                width: 100
                                icon.source: checked ? "images/edit-icon.svg" : "images/Check/Check.svg"
                                icon.height: 16
                                icon.width: 16
                                //text: checked ? "" : "Done "
                                checkable: true

                                checked: swipe.currentIndex == 1

                                onClicked: swipe.currentIndex = (checked ? 1 : 0)

                                //anchors.left: parent.left
                                //anchors.bottom: parent.bottom
                                //Layout.fillWidth: true
                                Layout.alignment: Qt.AlignHRight | Qt.AlignTop

                                //anchors.leftMargin: 30
                                //anchors.bottomMargin: 84
                                flat: true
                                highlighted: false //root.state == "PreviewPresetState"//true
                                //Material.background: Material.accent
                                Material.elevation: hovered ? 6 : 0

                                ToolTip.text: checked ? "Modify calibration preset" : "Apply changes"
                                ToolTip.delay: 1000
                                ToolTip.timeout: 5000
                                ToolTip.visible: hovered
                            }

                            RoundButton {
                                id: closePresetButton
                                icon.source: "images/Close.svg"
                                icon.height: 16
                                icon.width: 16
                                flat: true
                                visible: root.state == "EditPresetState"
                                onClicked: swipe.currentIndex = 1
                            }

                            RoundButton {
                                id: presetMoreButton
                                icon.source: "images/More.svg"
                                flat: true
                                visible: root.state == "PreviewPresetState"

                                Connections {
                                    target: presetMoreButton
                                    onClicked: presetMoreMenu.popup()
                                }
                                Menu {
                                    id: presetMoreMenu
//                                    MenuItem {
//                                        text: "Calibrate"
//                                        icon.source: "images/Play.svg"
//                                    }
//                                    MenuItem {
//                                        text: "Edit"
//                                        icon.source: "images/edit-icon.svg"
//                                    }

                                    MenuItem {
                                        text: "Save As..."
                                        icon.source: "images/Save.svg"
                                    }                                    
                                    MenuSeparator {}
                                    MenuItem {
                                        text: "Delete"
                                        icon.source: "images/Delete.svg"
                                    }
                                }
                            }
                        }
                    }

                    RowLayout {
                        id: tabbarSecondPlace
                        Layout.fillWidth: true
                        visible: root.width < 800
                    }
                    /////>>>>>>>>>>>>>>>
                }
            } //Pane presetPane

            SwipeView {
                id: swipe
                orientation: Qt.Vertical
                clip: true
                interactive: false

                Layout.fillWidth: true
                Layout.fillHeight: true

                PresetConfig{
                    id: presetConfigPage
                    swipe: parent
                    portsModel: portsModel
                    groupsModel: groupsModel1
                    activeGroup: tabBarH.currentIndex// + 1
                    anchors.left: parent.left
                    anchors.right: parent.right
                } //PresetConfig

                PresetPreview {
                    id: presetPreviewPage

                    anchors.left: parent.left
                    anchors.right: parent.right
                } //PresetPreview
            } //SwipeView

     /*
            Pane {
                id: portsPane

                Layout.fillHeight: false
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

                leftPadding: 40
                rightPadding: 40

                visible: root.state == "EditPresetState"

                ColumnLayout {
                    anchors.fill: parent

                    RowLayout {
                        id: portsSelectionInfo
                        Layout.fillWidth: true
                        //Layout.alignment: Qt.AlignHRight | Qt.AlignTop
                        height: swipe.currentIndex == 1 ? 0 : undefined
                        visible: swipe.currentIndex == 1 ? false : true

                        Image {
                            id: name
                            source: "images/Digits/001-countdown.svg"
                            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                            sourceSize: Qt.size(32, 32)
                            fillMode: Image.PreserveAspectFit
                        }
                        Label {
                            id: portsSelectionLabel
                            Layout.fillWidth: true
                            text: "<h3>Ports To Be Calibrated</h3><br>Select the Ports That Need to Be Calibrated by Clicking on Them Below:"

                            padding: 10

                            verticalAlignment: Text.AlignVCenter
                            wrapMode: Text.WordWrap
                            Layout.alignment: Qt.AlignLeft | Qt.AlignTop

                            //                            Connections {
                            //                                target: portsSelectionLabel
                            //                                Component.onCompleted: portsSelectionLabel.color = Material.color(Material.primary)
                            //                            }
                        }
                    }

                    Flow {
                        id: portsFlow
                        //anchors.fill: parent
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        spacing: 10

                        Repeater {
                            id: portsRepeater
                            model: portsModel
                            delegate: RoundButton {
                                id: portButton
                                property int activeGroup: tabBarH.currentIndex + 1//groupsSwipe
                                property bool inActiveGroup: model.group === activeGroup
                                property bool editMode: swipe.currentIndex == 0
                                property bool editModeHighlighted: highlightedInGroup && !inActiveGroup
                                property bool viewModeHighlighted: highlightedInGroup
                                                                   || inActiveGroup
                                property int portIndex: index + 1

                                //property bool flatValue: editMode ?
                                width: 46
                                height: 46
                                text: portIndex

                                /// State modification
                                checkable: !used || inActiveGroup //true
                                highlighted: editMode ? editModeHighlighted : viewModeHighlighted

                                layer.enabled: editMode ? editModeHighlighted : viewModeHighlighted
                                layer.effect: Glow {
                                    samples: 50
                                    radius: 27
                                    color: Material.accent
                                    transparentBorder: true
                                }
                                checked: editMode ? inActiveGroup : checked
                                //highlightedInGroup
                                //       || (editMode ? false : (group == groupsSwipe.currentIndex + 1))
                                flat: editMode ? (used && !inActiveGroup) : !used
                                Material.elevation: hovered ? 10 : 3
                                hoverEnabled: true

                                ToolTip {
                                    id: tooltip
                                    delay: 300
                                    timeout: 5000
                                    visible: hovered //&& !highlightedInGroup
                                    contentItem: Label {
                                        text: tooltip.text
                                        font: tooltip.font
                                        textFormat: Text.RichText
                                        wrapMode: Text.Wrap
                                    }

                                    Connections {
                                        target: tooltip
                                        Component.onCompleted: {
                                            var tooltipText = "<h3><img src = \"images/Calibration/port-m.svg\" width=\"24\" height=\"24\"> Uncalibrated port</h3> <br> <tt>Press to select for calibration</tt>"
                                            if (used) {
                                                var groupItem = groupsModel1.get(
                                                            group - 1)
                                                tooltipText = "<h3><img src = \"images/port_group.svg\"> " + groupItem.name + "</h3> <br><tt>Press to select group</tt> "
                                                var count = groupItem.calibration.count
                                                if (count > 0)
                                                    tooltipText += "<br> <style type=\"text/css\">
                                                                    .tg  {border-collapse:collapse;border-spacing:0;}
                                                                    .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
                                                                    overflow:hidden;padding:10px 5px;word-break:normal;}
                                                                    .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
                                                                    font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
                                                                    .tg .tg-1{background-color:#9b9b9b;border-color:#efefef;text-align:center;vertical-align:top}
                                                                    .tg .tg-2{background-color:#9b9b9b;text-align:center;vertical-align:top}
                                                                    </style>
                                                                    <table class=\"tg\">
                                                                    <thead>
                                                                    <tr>"

                                                for (var i = 0; i < count; i++) {
                                                    console.log(groupItem.calibration.get(
                                                                    i).type)
                                                    tooltipText += "<th class=\"tg-" + (i + 1)
                                                            + "\">" + groupItem.calibration.get(
                                                                i).type + "</th>"
                                                }
                                                tooltipText += "</tr></thead></table>"
                                            }
                                            tooltip.text = tooltipText
                                        }
                                    }
                                }

                                Connections {
                                    target: portButton
                                    onHoveredChanged: {
                                        if (used) {
                                            var model = portsModel
                                            for (var i = 0; i < model.rowCount(); i++) {
                                                if (model.get(i).group === group) {
                                                    model.get(i).highlightedInGroup
                                                            = portButton.hovered
                                                }
                                            }
                                        } else {
                                            //addGroupButtonH.highlighted = portButton.hovered
                                            //addGroupButtonH.flat = !portButton.hovered
                                        }

                                    }
                                    onReleased: {
                                        var modelList = portsModel
                                        var modelItem = modelList.get(
                                                    portIndex - 1)
                                        //!!!swipe.currentIndex = used ? 1 : 0
                                        if (used) {
                                            console.log("Press on USED port (in active group = "
                                                        + inActiveGroup + ")")
                                            console.log("Checkable=" + checkable)
                                            if (!inActiveGroup) {
                                                console.log("Swith to another group")
                                                tabBarH.currentIndex = group - 1
                                                //checkable = true
                                                //checked = true
                                                //                                                for (var i = 0; i < modelList.rowCount(); i++) {
                                                //                                                    if (modelList.get(i).group === group) {
                                                //                                                        checked = true
                                                //                                                    }
                                                //                                                }
                                            } else {
                                                console.log("Remove from group")
                                                modelItem.used = false
                                                modelItem.group = undefined
                                            }
                                        } else {
                                            console.log("Press on UNUSED port (group = "
                                                        + activeGroup + ")")
                                            console.log("Add to group")
                                            modelItem.used = true
                                            modelItem.group
                                                    = activeGroup //groupsSwipe.currentIndex + 1
                                            //checked = false
                                        }
                                    }
                                    onCheckedChanged: {
                                        console.log("checked changed to " + checked)
                                    }
                                }
                            }
                        }
                    }

                    RowLayout {
                        id: portsSelecionButtons
                        Layout.fillWidth: true
                        //Layout.alignment: Qt.AlignHRight | Qt.AlignTop
                        height: swipe.currentIndex == 1 ? 0 : undefined
                        visible: swipe.currentIndex == 1 ? false : true

                        Button {
                            id: selectAllButon
                            flat: true
                            highlighted: true
                            text: "Select All Available"

                            icon.source: "images/select_all.svg"

                            Connections {
                                target: selectAllButon
                                onClicked: {
                                    var model = portsModel
                                    for (var i = 0; i < model.rowCount(); i++) {
                                        if (!model.get(i).used)
                                            portsRepeater.itemAt(
                                                        i).checked = true
                                    }
                                }
                            }
                        }
                        Button {
                            id: unselectButton
                            flat: true
                            text: "Unselect"
                            Connections {
                                target: unselectButton
                                onClicked: {
                                    for (var i = 0; i < portsRepeater.count; i++) {
                                        portsRepeater.itemAt(i).checked = false
                                    }
                                }
                            }
                        }
                    }
                    /// Tab ViewSection
                }
            }

            MenuSeparator{
                Layout.fillWidth: true
                leftPadding: 40
                rightPadding: 40
            }

            SwipeView {
                id: swipe
                orientation: Qt.Vertical
                clip: true

                Layout.fillWidth: true
                Layout.fillHeight: true

                Pane {
                    id: calsPane

                    leftPadding: 40
                    rightPadding: 40
                    Material.elevation: 5

                    ColumnLayout {
                        anchors.fill: parent

                        RowLayout {
                            id: calsPaneHeader
                            Layout.fillWidth: true

                            Image {
                                source: "images/Digits/002-countdown-1.svg"
                                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                                sourceSize: Qt.size(32, 32)
                                fillMode: Image.PreserveAspectFit
                            }

                            Label {
                                id: calibrationTypeLabel
                                text: "<h3>Type of Calibration</h3> <br>Select the Calibration <b>TYPE</b> and <b>ALGORITHM</b> Below:"
                                padding: 10

                                Layout.fillWidth: true
                                verticalAlignment: Text.AlignVCenter
                                wrapMode: Text.WordWrap
                                Layout.alignment: Qt.AlignLeft | Qt.AlignTop

                                //                            Connections {
                                //                                target: calibrationTypeLabel//this
                                //                                Component.onCompleted: calibrationTypeLabel.color = Material.color(Material.accent)
                                //                            }
                            }
                        }


                        CheckBox {
                            id: groupedMode
                            text: "Group Types"
                        }

                        Flow {
                        //RowLayout {
                            id: flow2
                            //anchors.fill: parent
                            spacing: 10

                            Layout.fillWidth: true
                            Layout.fillHeight: true

                            CalModels {
                                model: calsModel
                                grouped: groupedMode.checked
                            }
                        }
                    }
                }

                Pane {
                    id: groupsPane
                    Material.elevation: 5

                    ColumnLayout {
                        anchors.fill: parent

//                        Image {
//                            source: "images/Graphics/Graphics.svg"
//                            Layout.alignment: Qt.AlignCenter
//                        }

                        //SwipeView {
                        Flow {
                        //ColumnLayout{
                            spacing: 10
                            Layout.leftMargin: 40
                            Layout.rightMargin: 40

                            id: groupsSwipe
                            Layout.fillHeight: true
                            Layout.fillWidth: true

                            //currentIndex: tabBarH.currentIndex
                            Repeater {
                                model: groupsModel1

                                delegate: Pane {
                                    id: pane

                                    //anchors.fill: parent
                                    Layout.fillHeight: true
                                    Layout.fillWidth: true
                                    Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom

                                    leftPadding: 30
                                    rightPadding: 30

                                    Material.elevation: 10
                                    Material.background: Material.dialogColor//Material.primary
                                    ColumnLayout {
                                        anchors.fill: parent

                                        RowLayout {
//                                            Image {
//                                                source: "images/Calibration/portsicon.svg"
//                                                width: 16
//                                                height:16
//                                                Layout.alignment: Qt.AlignHCenter// | Qt.AlignBottom
//                                            }

                                            Label {
                                                text: "<h3><img src=\"images/Calibration/portsicon.svg\" align=\"middle\" width=\"16\" height=\"16\"> " + name + "</h3>"
                                                //text: "<h3> <img src=\"images/Add.svg\" width=\"24\" height=\"24\"> " + name + "</h3>"
                                                Layout.fillWidth: true
                                                Layout.alignment: Qt.AlignHCenter// | Qt.AlignBottom
                                                verticalAlignment: Text.AlignVCenter
                                                wrapMode: Text.WordWrap
                                                textFormat: Text.RichText
                                                font.capitalization: Font.AllUppercase
                                            }
                                        }

                                        RowLayout {
                                            //spacing: -10
                                            Repeater {
                                                model: ports
                                                RoundButton {
                                                    text: port
                                                    Material.elevation: 1
                                                    Material.background: Material.background//backgroundDimColor
                                                }
                                            }
                                        }

                                        RowLayout {
                                            spacing: 5
                                            Repeater {
                                                model: calibration
                                                Button {
                                                    id: control
                                                    width: 100
                                                    text: type
                                                    //radius: 20
                                                    Material.elevation: 1
                                                    //enabled: false
                                                    //highlighted: true

                                                    background: Rectangle {
                                                            color: control.Material.primary//backgroundColor
                                                            radius: 20//control.Material.elevation > 0 ? control.radius : 0

                                                            layer.enabled: control.enabled && control.Material.elevation > 0
                                                            layer.effect: ElevationEffect {
                                                                elevation: control.Material.elevation
                                                            }
                                                        }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

//                        RowLayout {
//                            Layout.fillWidth: true

//                            Button {
//                                Layout.preferredWidth: 200
//                                Layout.preferredHeight: 70
//                                highlighted: false
//                                Material.background: Material.primary
//                                text: !hovered ? "Calibrate" : "Calibrate<br><code>6 ports</code>"
//                            }
//                            CheckBox {
//                                text: "All channels"
//                            }
//                        }
                    }
                }
            }

            MenuSeparator{
                Layout.fillWidth: true
                leftPadding: 40
                rightPadding: 40
            }

            Pane {
                id: calPlane
                Layout.fillWidth: true
            }

        */
        } //ColumnLayout

    } //Item


    } // SplitView

    states: [
        State {
            name: "PreviewPresetState"
            PropertyChanges {
                target: presetPane
                bg: root.Material.background
            }

//            PropertyChanges {
//                target: pane
//                rightPadding: 40
//                leftPadding: 40
//            }
        },
        State {
            name: "EditPresetState"
            PropertyChanges {
                target: presetPane
                bg: root.Material.primary
            }
        },
        State {
            name: "NewScriptState"
            PropertyChanges {
                target: presetPane
                Material.background: Material.primary
            }
        }
    ]

    transitions: Transition {
        ColorAnimation {
            //properties: "presetPane.Material.background, color, bg"
            properties: "bg"
            duration: 300
            easing.type: Easing.Linear
        }
    }

    state: swipe.currentIndex == 0 ? "EditPresetState" : "PreviewPresetState"
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.5}
}
##^##*/

