import QtQuick 2.12
import QtQml.Models 2.12

DelegateModel {
    id: delegateModel

    property var lessThan: function(left, right) { return true; }
    property var filterAcceptsItem: function(item) { return true; }

    function update() {
        var visible = [];
        for (var i = 0; i < items.count; ++i) {
            var item = items.get(i);
            if (filterAcceptsItem(item.model)) {
                item.inVisible = true
                visible.push(item);
            } else
                item.inVisible = false
        }

        visible.sort(function(a, b) {
            return lessThan(a.model, b.model) ? -1 : 1;
        });

        for (i = 0; i < visible.length; ++i) {
            item = visible[i];
            if (item.visibleIndex !== i) {
                visibleItems.move(item.visibleIndex, i, 1);
            }
        }
    }

    items.onChanged: update()
    onLessThanChanged: update()
    onFilterAcceptsItemChanged: update()

    groups: DelegateModelGroup {
        id: visibleItems

        name: "visible"
        includeByDefault: false
    }

    filterOnGroup: "visible"
}
