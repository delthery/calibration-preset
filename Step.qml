import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Material.impl 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.15
import QtQml.Models 2.12
import Qt.labs.qmlmodels 1.0


Item {
    id: root

    property alias index: indicator.text
    property alias name: label.name
    property alias description: label.description
    property alias content: content.body
    property alias actions: content.actions

    property bool enabled: false
    property bool expanded: false
    property bool done: false
    property bool checked: false
    //property ExclusiveGroup exclusiveGroup: null

    signal next
    signal previous
    signal complete
    signal reset

    //width: 950
    anchors.left: parent.left
    anchors.right: parent.right

    implicitHeight: Math.max(indicator.height, label.height) + Math.max(content.height, line.height)

//    Rectangle {
//        id: testRectangle1
//        anchors.fill: root
//        color: "transparent"
//        border.color: Material.accent
//    }

    GridLayout {
        //anchors.fill: parent
        anchors.left: parent.left
        anchors.right: parent.right

        RoundButton {
            id: indicator
            Layout.column: 0
            Layout.row: 0
            Layout.alignment: Qt.AlignCenter

            onClicked: root.state == "Disabled" ? root.state = "Enabled" :
                       root.state == "Enabled" ? root.state = "Error" :
                       root.state == "Error" ? root.state = "Done": root.state = "Disabled"
        }
        StepLabel {
            id: label
            Layout.column: 1
            Layout.row: 0
            Layout.fillWidth: true
            Layout.topMargin: 0
            Layout.bottomMargin: 0
        }
        ToolSeparator {
            id: line
            Layout.column: 0
            Layout.row: 1
            Layout.fillHeight: true;
            Layout.alignment: Qt.AlignCenter
            Layout.topMargin: 0
            Layout.bottomMargin: 0
            Layout.minimumHeight: 10
            visible: root.parent.data[root.parent.data.length-1] !== root
        }
        StepBody {
            id: content;
            Layout.column: 1
            Layout.row: 1
            Layout.fillWidth: true;
            Layout.fillHeight: true
            Layout.topMargin: 0
            Layout.bottomMargin: 0
        }
    }

    state: "Disabled"

    states: [
        State {
            name: "Disabled"
            PropertyChanges {
                target: indicator
                icon.source: "images/locked.svg"
                display: AbstractButton.IconOnly
                flat: true
            }
            PropertyChanges { target: label;        showDescription: false}
            PropertyChanges { target: content;      visible: false ;        height: 10}
            PropertyChanges { target: line;         implicitHeight: 35 }
        },
        State {
            name: "Enabled"
            PropertyChanges {
                target: indicator
                icon.source: ""
                display: AbstractButton.TextOnly
                highlighted: true
            }
            PropertyChanges { target: label;        showDescription: true}
            PropertyChanges { target: content;      visible: true }
        },
        State {
            name: "Error"
            PropertyChanges {
                target: indicator
                icon.source: "images/Warining.svg"
                display: AbstractButton.IconOnly
                //highlighted: true
                flat: true
                //Material.accent: Material.Red
                icon.color: Material.color(Material.Red)
            }
            PropertyChanges {
                target: label
                Material.foreground: Material.Red
            }
        },
        State {
            name: "Done"
            PropertyChanges {
                target: indicator
                icon.source: "images/Check/Check.svg"
                display: AbstractButton.IconOnly
                highlighted: true
            }
            PropertyChanges { target: label;        showDescription: false}
            PropertyChanges { target: content;      visible: false ;        height: 10}
            PropertyChanges { target: content;      showActions: false; enabled: false}
            PropertyChanges { target: line;         height: 35  }
        }

    ]

//    onExclusiveGroupChanged: {
//        if (exclusiveGroup)
//            exclusiveGroup.bindCheckable(root)
//    }

    onExpandedChanged: {
        state = expanded ? "Enabled" : "Done"
    }

    onReset: state = "Disabled"

    component StepIndicator: RoundButton {
        text: "0"
        Material.elevation: 1
    }

    component StepLabel: Label {
        id: stepLabel
        property string name : "default name"
        property string description: "default description"
        property bool showDescription: true

        readonly property string shortPattern: "<b>%1<b/>"
        readonly property string fullPattern: shortPattern + "<br><code><font color=\"%3\">%2</font></code>"

        property string message: showDescription ? fullPattern : shortPattern

        text: message.arg(name).arg(description).arg(Material.secondaryTextColor)
        padding: 10

        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.WordWrap
        textFormat: Text.RichText
    }

    component StepBody: ColumnLayout {
        default property Item body
        property list<Item> actions
        property bool showActions: true


        implicitHeight: stepBodyPane.implicitHeight + actionsRow.implicitHeight

        Pane {
            id: stepBodyPane

            //Layout.fillHeight: false
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            //Layout.leftMargin: 10

//            contentWidth: width
//            contentHeight:

            implicitHeight: body.implicitHeight// + padding
            //implicitHeight: implicitContentHeight + topPadding + bottomPadding

            //leftPadding: 40
            //rightPadding: 40
            padding: 30

            data: body
            Material.elevation: 5
//            ColumnLayout {
//                anchors.fill: parent
//                data: body
//            }

            onImplicitContentHeightChanged: console.log(">>> Content Height: " + implicitContentHeight)

            Connections {
                target: stepBodyPane
                onBodyChanged: {
                    //if(!data) return
                    body.padding = 30;
                    body.anchors.fill = body.parent;
                    console.log("!!!!!!!!!!!!!onDataChnged:" + data)
                }
                Component.onCompleted: {
                    body.padding = 30;
                    body.anchors.fill = body.parent;
                    console.log("!!!!!!!!!!!!!onCompleted:" + data)
                }
            }

//            onDataChanged: {
//                if(!data) return
//                data.padding = 30
//                data.anchors.fill = data.parent//Qt.binding(function () { return data.parent })
////                implicitHeight = Qt.binding(function () {
////                               return dat
////                          })
//            }
        }

        RowLayout {
            id: actionsRow
            visible: showActions
            Layout.fillWidth: true
            implicitHeight: data[0].implicitHeight
            //Layout.alignment: Qt.AlignHRight | Qt.AlignTop
            data: actions
        }
    }
}
