import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Material.impl 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.15
import QtQml.Models 2.12
import Qt.labs.qmlmodels 1.0


ScrollView {
    id: scrollView

    default property alias data: stepperColumn.data

    property int selectedIndex: 0
    property Item selected: (selectedIndex >= 0) ? stepperColumn.data[selectedIndex] : undefined

    signal next
    signal prev
    signal reset

    ColumnLayout {
        id: stepperColumn
        anchors.left: parent.left
        anchors.right: parent.right        
    }

    onNext: {
        stepperColumn.data[selectedIndex].expanded = false

        if( selectedIndex + 1 < stepperColumn.data.length) {
            selectedIndex = selectedIndex + 1
            stepperColumn.data[selectedIndex].expanded = true
        } else {
            createFinishStep()
        }

    }

    onPrev: {
        if( selectedIndex - 1 >= 0) {
            stepperColumn.data[selectedIndex].expanded = false
            selectedIndex = selectedIndex - 1
            stepperColumn.data[selectedIndex].expanded = true
        }
    }

    onReset: {
        //finishStep.destroy();
        selectedIndex = 0
        stepperColumn.data[0].expanded = true;
        stepperColumn.data[stepperColumn.data.length - 1].destroy();
        for(var i = 1; i < stepperColumn.data.length - 1; i++)
            stepperColumn.data[i].reset()
    }

    Component.onCompleted: {
        stepperColumn.data[selectedIndex].expanded = true

        createSpriteObjects();
    }

    component FinishStep: RowLayout {
        Label {
            text: "All steps completed - you're finished"
        }
        Button {
            id: resetButton
            text: "Reset"
        }
    }

    function createFinishStep() {
        console.log("Creating finish step")
        component = Qt.createQmlObject("import QtQuick 2.15
                                        import QtQuick.Controls 2.12
                                        import QtQuick.Controls.Material 2.12
                                        import QtQuick.Controls.Material.impl 2.12
                                        import QtQuick.Layouts 1.12
                                        ColumnLayout {
                                        Layout.fillWidth: true
                                        id: finishStep
                                        Label { text: \"<b>All steps completed - you're finished</b>\" }
                                        Button { id: resetButton; text: \"Reset\"; onClicked: {scrollView.reset()} }}",
                                       stepperColumn, "finishStep");
//        if (component.status === Component.Ready)
//            finishCreation();
//        else
//            component.statusChanged.connect(finishCreation);
    }

//    function finishCreation() {
//        if (component.status === Component.Ready) {
//            sprite = component.createObject(appWindow, {x: 100, y: 100});
//            if (sprite == null) {
//                // Error Handling
//                console.log("Error creating object");
//            }
//        } else if (component.status === Component.Error) {
//            // Error Handling
//            console.log("Error loading component:", component.errorString());
//        }
//    }
}
