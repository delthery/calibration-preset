import QtQuick 2.15

Item {
    required property string path
    property string value: ""
    property int port1: 0
    property int port2: 1

    property string str

    function read(fileUrl) {
        var request = new XMLHttpRequest();
        request.open("GET", fileUrl, false);
        request.send(null);
        return request.responseText;
    }
    function update() {
        value = "data:image/svg+xml;utf8," + str.arg("PORT " + port1).arg("PORT " + port2)
    }

    Component.onCompleted: {
        str = read(path)
        value = "data:image/svg+xml;utf8," + str.arg("PORT " + port1).arg("PORT " + port2)
        //console.log(value)
    }

    onPort1Changed: update()
    onPort2Changed: update()
}
